import { Offcanvas } from 'bootstrap';
import { initPlayer, setAlbum } from './renderer/player';

let albums: Album[];
let offCanvas: Offcanvas | undefined;

async function getMessages() {
  const response = await window.api.ping();
  console.log('ping response', response);
}

async function getAlbumDirectories() {
  albums = await window.api.getAlbums(navigator.onLine);
	const messages = await getMessages();
	console.log('messages', messages);
  console.log('getAlbums', albums);

	// Create the grid.
	const albumsGrid = document.getElementById('albums');
	if (albumsGrid) {
		albumsGrid.innerHTML = albums.reduce((accumulator, album) => `${accumulator}
			<div id="${album.localDirectoryName}" class="album-card col position-relative">
				<div class="album-cover ratio ratio-1x1">
					<span>${album.release}</span>
				</div>
				<div class="album-info position-absolute bottom-0 start-0 end-0 top-0 p-2 bg-black bg-opacity-50 text-white flex-column">
					<div class="flex-grow-1">
						<h4></h4>
						<p></p>
					</div>
					<div class="d-flex justify-content-center">
						<button type="button" class="btn btn-link">
							<span class="material-icons-outlined fs-1 text-white">play_arrow</span>
						</button>
					</div>
				</div>
			</div>`,
			'');
	}

	// Get the cover images.
	albums.forEach(async (album) => {
		const { localDirectoryName, cover } = album;
		if (localDirectoryName && cover) {
			const base64String = await window.api.getImage(localDirectoryName, cover);
			const gridCell = document.getElementById(localDirectoryName);
			if (gridCell) {
				const albumCoverEl = gridCell.querySelector('.album-cover');
				if (albumCoverEl) {
					albumCoverEl.innerHTML = `<img src="data:image/png;base64,${base64String}" loading="lazy" />`;
				}

				const playButton = gridCell.querySelector('button');
				playButton?.addEventListener('click', async () => {
					initPlayer();
					showAlbum(localDirectoryName);
				})
			}
		}
	});
}

async function init() {
	const offcanvasEl = document.querySelector('.offcanvas');
	if (offcanvasEl) {
		offCanvas = new Offcanvas(offcanvasEl);
	}

	const openPlayerEl = document.querySelector('.nav-open-player');
	openPlayerEl?.addEventListener('click', () => offCanvas?.show());

	const sortDropdownEl = document.getElementById('sort-type');
	sortDropdownEl?.addEventListener('click', (e) => {
		const sortType = (e.target as HTMLElement)?.dataset.sort;
		if (sortType) {
			sort(sortType);
		}
	});

	await getAlbumDirectories();
	sort('artist');
}

/**
 * Reorder the albums in the grid after the order was changed.
 */
function reorderAlbumsGrid(sortType: string | undefined) {
	const albumsGrid = document.getElementById('albums');
	if (albumsGrid) {
		albums.forEach((album) => {
			const albumEl = document.getElementById(album.localDirectoryName);
			if (albumEl) {
				const headerEl = albumEl.querySelector('h4');
				const paragraphEl = albumEl.querySelector('p');

				if (headerEl && paragraphEl && album.discogs) {
					const artists = album.discogs.artists.map((a) => a.name).join(', ');
					switch (sortType) {
						case 'artist':
							headerEl.textContent = artists;
							paragraphEl.innerHTML = `${album.discogs.title} (${album.discogs.year})<br>
								${album.discogs.labels[0].name} (${album.discogs?.labels[0].catno})`;
							break;

						case 'album':
							headerEl.textContent = album.discogs.title;
							paragraphEl.innerHTML = `${artists} (${album.discogs.year})<br>
								${album.discogs.labels[0].name} (${album.discogs?.labels[0].catno})`;
							break;

						case 'label':
							headerEl.textContent = album.discogs.labels[0].name;
							paragraphEl.innerHTML = `${artists} (${album.discogs.year})<br>
								(${album.discogs?.labels[0].catno})`;
							break;

						case 'catnr':
							headerEl.textContent = album.discogs.labels[0].catno;
							paragraphEl.innerHTML = `${artists}<br>
								${album.discogs.labels[0].name} (${album.discogs.year})`;
							break;
					}
				}



				albumsGrid.appendChild(albumEl);
			}
		});
	}
}

/**
 * Open the offscreen and show the player with tracklist.
 */
async function showAlbum(localDirectoryName: string) {
	const album = albums.find((a) => a.localDirectoryName === localDirectoryName);

	if (!album || !offCanvas) {
		return;
	}

	offCanvas.show();

	setAlbum(album);

	console.log('album', album);

	// show album cover
	const coverEl = document.querySelector('.player-album-cover');
	if (coverEl && album.cover) {
		const base64String = await window.api.getImage(localDirectoryName, album.cover);
		coverEl.innerHTML = `<img src="data:image/png;base64,${base64String}" />`;
	}

	// show album information next to cover
	const infoEl = document.querySelector('.player-album-info');
	if (infoEl && album.discogs) {
		const { artists, labels, year } = album.discogs;
		const artistsList = artists.reduce((accumulator, artist, index) => {
			return `${ accumulator }${ index > 0 ? ' & ' : '' } ${artist.name}`;
		}, '');
		const yearString = year ? ` (${year})` : '';
		infoEl.innerHTML = `${album.discogs.title}${yearString}<br>
			${artistsList}<br>
			${labels[0].name} (${labels[0].catno})`;
	}
}

/**
 * Sort the grid.
 */
function sort(sortType: string | undefined) {
	// set the sort dropdown label
	const sortDropdownEl = document.getElementById('sort-type');
	if (sortDropdownEl) {
		// set the sort dropdown label
		const itemEl = sortDropdownEl.querySelector(`.dropdown-item[data-sort=${sortType}]`);
		const btnEl = sortDropdownEl.querySelector('.dropdown-toggle');
		if (itemEl && btnEl) {
			btnEl.textContent = itemEl.textContent;
		}

		// set the dropdown item as selected
		sortDropdownEl.querySelectorAll('.dropdown-item[data-sort]').forEach((itemEl) => {
			const el = itemEl as HTMLElement;

			if (el.dataset.sort === sortType) {
				el.classList.add('active');
				el.setAttribute('aria-current', 'true');
			} else {
				el.classList.remove('active');
				el.removeAttribute('aria-current');
			}
		});
	}

	// perform the sort
	if (albums) {
		switch (sortType) {
			case 'artist':
				albums.sort((a: Album, b: Album) => {
					const nameA = a.discogs ? a.discogs.artists[0].name : a.artistName;
					const nameB = b.discogs ? b.discogs.artists[0].name : b.artistName;
					return nameA === nameB ? 0 : nameA > nameB ? 1 : -1;
				});
				break;

			case 'album':
				albums.sort((a: Album, b: Album) => {
					const nameA = a.discogs ? a.discogs.title : a.release;
					const nameB = b.discogs ? b.discogs.title : b.release;
					return nameA === nameB ? 0 : nameA > nameB ? 1 : -1;
				});
				break;

			case 'label':
				albums.sort((a: Album, b: Album) => {
					const nameA = a.discogs ? a.discogs.labels[0].name : '';
					const nameB = b.discogs ? b.discogs.labels[0].name : '';
					return nameA === nameB ? 0 : nameA > nameB ? 1 : -1;
				});
				break;

			case 'catnr':
				albums.sort((a: Album, b: Album) => {
					const nameA = a.discogs ? a.discogs.labels[0].catno : a.catalogNumber;
					const nameB = b.discogs ? b.discogs.labels[0].catno : a.catalogNumber;
					return nameA === nameB ? 0 : nameA > nameB ? 1 : -1;
				});
				break;
		}
		reorderAlbumsGrid(sortType);
	}
}

init();
