import { Dirent } from 'fs';
import { rootPath } from 'electron-root-path';
import { readFile, readdir, writeFile } from 'fs/promises';
import path from 'node:path';
import discogs from 'disconnect';

let settings: { albums_directory: string, albums_json: string, discogs_user_token: string };

let messages: string[] = [];

/**
 * Get initial album data by splitting the dirctory names.
 */
const getAlbumData = (dirNames: string[]) => {
	return dirNames.map((name) => ({
		audioFileNames: [],
		localDirectoryName: name,
		artistName: name.split('] ')[1].split(' - ')[0],
		catalogNumber: name.substring(1).split('] ')[0],
		release: name.split('] ')[1].split(' - ')[1].split(' (')[0],
		discogs: undefined,
		discogsId: undefined,
		cover: undefined,
	}));
};

/**
 * Get all album directories in the main albums directory.
 */
const getAlbumDirectoryNames = async () => {
	const source = path.resolve(rootPath, settings.albums_directory);
	const all: Dirent[] = await readdir(source, { withFileTypes: true, recursive: false });
	const dirs: Dirent[] = all.filter(dirent => dirent.isDirectory());
	const dirNames = dirs.map(dirent => dirent.name);
	return dirNames;
};

/**
 * Find the cover image files in the album directories.
 */
const getAlbumCoverImages = async (albumData: Album[]) => {
	for (const album of albumData) {
		const source = path.resolve(rootPath, settings.albums_directory, album.localDirectoryName);
		const all: Dirent[] = await readdir(source, { withFileTypes: true, recursive: false });
		const fileNames = all.map(file => file.name);
		let coverJpg: string | undefined = fileNames.find((name) => name === 'cover.jpg');
		if (!coverJpg) {
			coverJpg = fileNames.find((name) => /^R.+.jpg$/.test(name));
		}
		album.cover = coverJpg;
	}
	return albumData;
};

/**
 * Get a list of all mp3 files for each album directory.
 */
const getAlbumAudioFileNames = async (albumData: Album[]) => {
	for (const album of albumData) {
		const source = path.resolve(rootPath, settings.albums_directory, album.localDirectoryName);
		const all: Dirent[] = await readdir(source, { withFileTypes: true, recursive: false });
		const audioFileNames = all.reduce((accumulator, dirent) => (
			(dirent.isFile() && path.extname(dirent.name).toLowerCase() === '.mp3')
				? [...accumulator, dirent.name]
				: accumulator
			), [] as string[]);
		album.audioFileNames = audioFileNames;
	};
	return albumData;
}

/**
 * Get a list of albums that have a discogs id but no discogs data
 */
const getAlbumsToGetByDiscogsId = (albumData: Album[]) => (
	albumData.reduce((accumulator, album) => {
		if (album.discogsId && !album.discogs) {
			album.discogs = undefined;
			return [...accumulator, album];
		}
		return accumulator;
	}, [] as Album[])
);

/**
 * Get album data from Discogs.com.
 */
const getDiscogsData = async (albumData: Album[], isByDiscogsId: boolean = false) => {
	const db = new discogs.Client(undefined, { userToken: 'lpZWfNuiTqRcHrfTwolFtngxYAdKFSdWJUnQktYl' }).database();
	for (const album of albumData) {
		console.log(album.artistName.normalize('NFC'));
		console.log(album.release.normalize('NFC'));
		let searchResult: any;

		if (isByDiscogsId) {
			if (album.discogsId) {
				searchResult = await db.getRelease(album.discogsId);
				if (searchResult) {
					album.discogs = searchResult;
				}
			}
		} else {
			searchResult = await db.search('', {
				artist: album.artistName.normalize('NFC'),
				release_title: album.release.normalize('NFC'),
				type: 'release',
			});

			if (searchResult.results && searchResult.results.length > 0 && searchResult.results[0].resource_url) {
				const releaseData = await fetch(searchResult.results[0].resource_url);
				if (releaseData.status === 200) {
					const releaseJson = await releaseData.json();
					album.discogs = releaseJson;
				}
			}
		}
	}
	return albumData;
}

export const getMessages = () => messages;

/**
 * Get a list of the new albums directories that are not in the albums JSON yet.
 */
const getNewAlbumsOnly = (dirNames: string[], existingAlbums: Album[]) => {
	return dirNames.reduce((accumulator, dirName) => {
		if (existingAlbums.find((album) => album.localDirectoryName === dirName)) {
			return accumulator;
		}
		return [...accumulator, dirName];
	}, [] as string[]);
};

/**
 * Load the albums JSON file.
 */
const readAlbumsFile = async () => {
	const fileLocation = path.resolve(rootPath, settings.albums_json);
	try {
		const file = await readFile(fileLocation, 'utf8');
		const data = JSON.parse(file);
		return data as Album[];
	} catch (err) {
		return [];
	}
};

/**
 * Load the settings JSON file.
 */
const readSettingsFile = async (isPackaged: boolean) => {
	const settingsPath = isPackaged ? '../settings.json' : './settings_dev.json';
	const fileLocation = path.resolve(rootPath, settingsPath);

	try {
		const file = await readFile(fileLocation, 'utf8');
		settings = JSON.parse(file);
		return;
	} catch (err) {
		return;
	}
};

const writeAlbumsFile = async (discogsData: Album[]) => {
	const fileLocation = path.resolve(rootPath, settings.albums_json);
	await writeFile(fileLocation, JSON.stringify(discogsData, null, 4));
};

/**
 * Entry point.
 */
export const getAlbums = async (isOnline: boolean, isPackaged: boolean) => {
	await readSettingsFile(isPackaged);
	let existingAlbums = await readAlbumsFile();
	let updatedAlbums: Album[] = [];

	if (isOnline) {
		const dirNames = await getAlbumDirectoryNames();

		// handle new album directories
		const newAlbumDirs = getNewAlbumsOnly(dirNames, existingAlbums);
		if (newAlbumDirs.length) {
			const albumData = getAlbumData(newAlbumDirs);
			const albumDataWithCovers = await getAlbumCoverImages(albumData);
			const albumDataWithAudioFileNames = await getAlbumAudioFileNames(albumDataWithCovers);
			const discogsData = await getDiscogsData(albumDataWithAudioFileNames);
			updatedAlbums = [...existingAlbums, ...discogsData];
		}

		// handle albums with a Discogs ID but without Discogs data.
		const albumsToGetByDiscogsId = getAlbumsToGetByDiscogsId(existingAlbums)
		if (albumsToGetByDiscogsId.length) {
			const discogsAlbums = await getDiscogsData(albumsToGetByDiscogsId, true);

			updatedAlbums = existingAlbums.map((a) => {
				const updatedAlbum = discogsAlbums.find((a2) => a2.localDirectoryName === a.localDirectoryName);
				return updatedAlbum ? updatedAlbum : a;
			});
		}

		// when changed save the albums data
		if (updatedAlbums.length && (newAlbumDirs.length || albumsToGetByDiscogsId.length)) {
			await writeAlbumsFile(updatedAlbums);
		}
	}

	return updatedAlbums.length ? updatedAlbums : existingAlbums;
};

export const getAudioFile = async (localDirectoryName: string, audioFileName: string) => {
	const fileLocation = path.resolve(rootPath, settings.albums_directory, localDirectoryName, audioFileName);
	const buffer = await readFile(fileLocation);
	return buffer;
};

export const getImage = async (localDirectoryName: string, coverFile: string) => {
	const fileLocation = path.resolve(rootPath, settings.albums_directory, localDirectoryName, coverFile);
	const base64String = await readFile(fileLocation, { encoding: 'base64' });
	return base64String;
};
