import { contextBridge, ipcRenderer } from 'electron';

contextBridge.exposeInMainWorld('api', {
  ping: () => ipcRenderer.invoke('ping'),
  getAlbums: (isOnline: boolean, isPackaged: boolean, appPath: string) => ipcRenderer.invoke('albums', isOnline, isPackaged, appPath),
  getAudioFile: (localDirectoryName: string, audioFileName: string) => ipcRenderer.invoke('audio', localDirectoryName, audioFileName),
  getImage: (localDirectoryName: string, coverFile: string) => ipcRenderer.invoke('image', localDirectoryName, coverFile),
})
