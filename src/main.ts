import { BrowserWindow, app, ipcMain } from 'electron';
import path from 'node:path';
import { getAlbums, getAudioFile, getImage, getMessages } from './main/albums';

const createWindow = () => {
  const win = new BrowserWindow({
    width: 1200,
    height: 700,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  win.loadFile('index.html');
}

app.whenReady().then(() => {
  ipcMain.handle('ping', () => getMessages());
  ipcMain.handle('albums', async (event, isOnline: boolean) => await getAlbums(isOnline, app.isPackaged));
  ipcMain.handle('audio', async (event, localDirectoryName: string, audioFileName: string) => await getAudioFile(localDirectoryName, audioFileName));
  ipcMain.handle('image', async (event, localDirectoryName: string, coverFile: string) => await getImage(localDirectoryName, coverFile));
  createWindow();
});

app.on('activate', () => {
	if (BrowserWindow.getAllWindows().length === 0) {
		createWindow();
	}
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
		app.quit();
	}
});

// app.dock.setIcon('assets/img/muziekspeler-mac.png');
