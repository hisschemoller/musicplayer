import { secondsToTime } from './util';

type PLAY_STATE = 'STOP' | 'PLAY' | 'PAUSE' | 'CONTINUE';

let album: Album;
let audioContext: AudioContext | undefined;
let audioBufferSourceNode: AudioBufferSourceNode | undefined;
let gainNode: GainNode;
let trackIndex: number | undefined;
let playState: PLAY_STATE = 'STOP';
let isPlaying: boolean = false;
let startTime: number = 0;
let position: number;
let duration: number;
let intervalId: NodeJS.Timeout;
let tracklistEl: HTMLDivElement;
let timeEl: HTMLDivElement;
let progressEl: HTMLProgressElement;

/**
 * If the track has finished start the next one in the album.
 */
function audioBufferEndedHandler() {
	if (audioBufferSourceNode && audioBufferSourceNode.buffer?.duration === duration) {
		if (trackIndex != undefined && album.discogs && trackIndex + 1 < album.discogs?.tracklist.length) {
			// a next track exists
			gotoAndPlayTrack(trackIndex + 1);
		} else {
			// last track of the album ended
			updatePlayState('STOP');
		}
	} else {
		// ended by another cause than track end, nothing to do here
	}
}

/**
 * Play another track of the album.
 */
function gotoAndPlayTrack(index: number) {
	trackIndex = index;

	updatePlayState('PLAY');
}

/**
 * Set a new album in the player.
 */
export function setAlbum(newAlbum: Album) {
	album = newAlbum;

	// fill the tracklist
	if (tracklistEl && album.discogs?.tracklist.length) {
		const isVariousArtists = album.discogs.artists[0].name === 'Various';
		const tracklistContent =  album.discogs.tracklist.reduce((accumulator, track, index) => {
			const title = isVariousArtists ? `${track.artists[0].name} - ${track.title}` : track.title;
			return `${ accumulator }
				<tr role="button" data-track-index="${index}">
					<td>${track.position}.</td>
					<td>${title}</td>
					<td class="text-end">${track.duration}</td>
				</tr>`;
		}, '');
		tracklistEl.innerHTML = tracklistContent;
	}

	// reset the player
	trackIndex = 0;
	updatePlayState('STOP');
}

/**
 * Initialise the player if it hasn't been yet.
 */
export function initPlayer() {
	if (audioContext) {
		return;
	}

	// audio
	audioContext = new AudioContext();

	gainNode = new GainNode(audioContext);
	gainNode.connect(audioContext.destination);

	// tracklist
	const el = document.querySelector('.player-tracklist');
	if (el) {
		tracklistEl = el as HTMLDivElement;
		tracklistEl.addEventListener('click', (e) => {
			// get clicked table row and set selected
			const tracklistRowEl = (e.target as HTMLElement).closest('[data-track-index]');

			// get track index
			const trackIndexString = (tracklistRowEl as HTMLElement)?.dataset.trackIndex;
			if (trackIndexString !== undefined ) {
				const index = parseInt(trackIndexString);
				gotoAndPlayTrack(index);
			}
		});
	}

	// play / pause button
	const playButtonEl = document.querySelector('.player-play');
	playButtonEl?.addEventListener('click', () => {
		if (isPlaying) {
			updatePlayState('PAUSE');
		} else {
			if (album) {
				if (position === 0) {
					updatePlayState('PLAY');
				} else {
					updatePlayState('CONTINUE');
				}
			} else {
				// no album, not playing, do nothing
			}
		}
	});

	// time display
	const playerTimeEl = document.querySelector('.player-time');
	if (playerTimeEl) {
		timeEl = playerTimeEl as HTMLDivElement;
	}

	// progress bar
	const playerProgressEl = document.querySelector('.player-progress');
	if (playerProgressEl) {
		progressEl = playerProgressEl as HTMLProgressElement;
		progressEl.addEventListener('mousedown', () => {
			updatePlayState('PAUSE');
		});
		progressEl.addEventListener('mouseup', (e) => {
			position = parseFloat((e.target as HTMLInputElement).value);
			updatePlayState('CONTINUE');
		});
		progressEl.addEventListener('input', (e) => {
			timeEl.textContent = secondsToTime(progressEl.value);
		});
	}
}

/**
 * Change between play, pause, continue and stop.
 */
async function updatePlayState(newState: PLAY_STATE) {
	playState = newState;
	isPlaying = playState === 'PLAY' || playState === 'CONTINUE';

	// update play button
	const icon = document.querySelector('.player-play .material-icons-outlined');
	// icon?.classList.toggle('bi-play-fill', playState === 'PAUSE' || playState === 'STOP');
	// icon?.classList.toggle('bi-pause-fill', playState === 'PLAY' || playState === 'CONTINUE');
	// icon?.classList.toggle('bi-stop-fill', playState === 'STOP' && false);

	if (icon) {
		switch (playState) {
			case 'PAUSE':
			case 'STOP':
				icon.textContent = 'play_arrow';
				break;
			case 'PLAY':
			case 'CONTINUE':
				icon.textContent = 'pause';
				break;
			default:
				icon.textContent = 'stop';
		}
	}

	if (trackIndex === undefined || !audioContext) {
		return;
	}

	// stop timer
	if (intervalId) {
		clearInterval(intervalId);
	}

	// stop audio
	if (audioBufferSourceNode) {
		audioBufferSourceNode.stop();
		audioBufferSourceNode.disconnect(gainNode);
		audioBufferSourceNode.removeEventListener('ended', audioBufferEndedHandler);
		audioBufferSourceNode = undefined;
	}

	if (playState === 'PLAY' || playState === 'CONTINUE') {
		if (playState === 'PLAY') {
			position = 0;
		}

		// start audio
		const audioFileName = album.audioFileNames[trackIndex];
		const uint8Array = await window.api.getAudioFile(album.localDirectoryName, audioFileName);
		const buffer = await audioContext.decodeAudioData(uint8Array.buffer as ArrayBuffer);
		audioBufferSourceNode = new AudioBufferSourceNode(audioContext, { buffer });
		audioBufferSourceNode.connect(gainNode);
		audioBufferSourceNode.start(0, position);
		audioBufferSourceNode.addEventListener('ended', audioBufferEndedHandler);
		startTime = audioContext.currentTime - position;
		duration = audioBufferSourceNode.buffer?.duration || 0;

		// start timer
		progressEl.setAttribute('max', duration.toString());
		timeEl.textContent = secondsToTime(0);

		intervalId = setInterval(() => updateProgressBar(), 1000);
		updateProgressBar();

		updateSelectedTrack();
	} else {
		if (playState === 'PAUSE') {
			position = audioContext.currentTime - startTime;
		} else if (playState === 'STOP') {
			position = 0;
		}
		progressEl.value = position;
		timeEl.textContent = secondsToTime(position);
	}
}

/**
 * Update the progress bar and time display while audio plays.
 */
function updateProgressBar() {
	if (audioContext) {
		position = audioContext.currentTime - startTime;
		progressEl.value = position;
		timeEl.textContent = secondsToTime(position);
	}
}

/**
 * Show the active track in the album table.
 */
function updateSelectedTrack() {
	// deselect previous selected row
	[...tracklistEl.querySelectorAll('.table-active')].forEach((el) => el.classList.remove('table-active', 'fst-italic'));

	// get clicked table row and set selected
	const tracklistRowEl = tracklistEl.querySelector(`[data-track-index="${trackIndex}"]`);
	tracklistRowEl?.classList.add('table-active', 'fst-italic');
}
