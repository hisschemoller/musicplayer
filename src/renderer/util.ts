export function timeToSeconds(time: string) {
	const [minutes, seconds] = time;
	return (parseInt(minutes) * 60) + parseInt(seconds)
}

export function secondsToTime(seconds: number | undefined) {
	if (seconds === undefined) {
		return '--:--';
	}

	const minutes = Math.floor(seconds / 60).toString().padStart(2, '0');
	const secs = Math.round(seconds % 60).toString().padStart(2, '0');
	return `${minutes}:${secs}`;
}
