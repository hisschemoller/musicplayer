declare module 'disconnect' {
	class Client {
		constructor(userAgent?: string, auth?: object, ...args: any[]);
		public database(): {
			/**
			 * Get release data
			 * @param {(number|string)} release - The Discogs release ID
			 * @param {function} [callback] - Callback
			 * @return {DiscogsClient|Promise}
			 */
			getRelease(release: (number | string), callback?: (err: Error, data: any) => void): DiscogsClient | Promise<any>;
			/**
			 * Search the database
			 * @param {string} query - The search query
			 * @param {object} [params] - Search parameters as defined on http://www.discogs.com/developers/#page:database,header:database-search
			 * @param {function} [callback] - Callback function
			 * @return {DiscogsClient|Promise}
			 */
			search(query: string, params?: object, callback?: (err: Error, data: any) => void, ...args: any[]): DiscogsClient | Promise<any>;
		}
	}

	class DiscogsClient {

	}
}

// declare module 'disconnect' {
// 	class Client {
// 		constructor();
// 		public database(): {
// 			getRelease(
// 				catalogNumber: string,
// 				callback: (err: Error, data: any) => void,
// 			): void,
// 		}
// 	}
// }

// declare module 'test' {
	// class MyClassA {
	// 		constructor();
	// 		public static staticMethodA(x: number, y: number): void;
	// 		public regularMethodA(z: string): string;
	// }

	// class MyClassB {
	// 		constructor();
	// 		public regularMethodB(q: string): string;
	// }

	// export interface classes {
	// 		ClassA: MyClassA;
	
	// 		MyClassB: MyClassB;
	// }
// }