declare interface Window {
	api: {
		ping: () => Promise<string[]>;
		getAlbums: (isOnline: boolean) => Promise<Album[]>;
		getAudioFile: (localDirectoryName: string, audioFileName: string) => Promise<Uint8Array>;
		getImage: (localDirectoryName: string, coverFile: string) => Promise<string>;
	}
}

declare interface Album {
	artistName: string;
	audioFileNames: string[];
	catalogNumber: string;
	cover: string | undefined;
	discogs: Discogs | undefined;
	discogsId: string | undefined;
	localDirectoryName: string;
	release: string;
}

declare interface Artist {
	name: string;
}

declare interface Discogs {
	artists: Artist[];
	country: string;
	id: number;
	labels: Label[];
	title: string;
	tracklist: Track[];
	year: number;
}

declare interface Label {
	name: string;
	catno: string;
}

declare interface Track {
	artists: Artist[];
	duration: string;
	position: string;
	title: string;
	type: string;
}
