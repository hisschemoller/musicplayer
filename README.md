# Musicplayer

```
usb_drive/
+-- player/
|   +-- musicplayer.app
|   +-- settings.json
|   +-- albums.json
+-- albums/
|   +-- album1/
|   +-- album2/
|   +-- ...
```

## Development

The Typescript type definitions are created with

```bash
npx -p typescript tsc ./node_modules/disconnect/**/*.js --declaration --allowJs --emitDeclarationOnly --outDir types
```

`npm run start` to start the app.

## App icon

Alle icons staan in `/assets/images/`. Voor Mac een .icns bestand
`muziekspeler-mac.icns`.<br/>
Converteer van .png naar .icns met https://vistatools.online/png-to-icns/<br/>
Een standaard Mac icoon is 1024px breed en hoog met 100px padding.<br/>
Toevoegen in `forge.config.js`:

```javascript
packagerConfig: {
	asar: true,
	icon: '/assets/img/muziekspeler-mac',
},
```

## Package

`npm run package-universal` for the big Mac Intel and M package.

## Verbinding met Discogs API.

Bij het starten van de app wordt vergeleken of er nieuwe mappen in `/albums`
zijn die nog niet in `albums.json` staan. Voor die mappen wordt de inhoud
gelezen en wordt een nieuwe item in `albums.json` gemaakt met basis informatie
die uit de map gehaald kan worden:

* Map naam heeft structuur [cat_no] artiest - album_titel (jaar)
* Alle audio bestanden zijn mp3's
* De afbeelding van de hoes heet `cover.jpg` of begint met hoofdletter 'R' (want
dan is het een afbeelding die gedownload is van Discogs).

Na verzamelen van de basis informatie wordt via de Discogs API verdere data
opgehaald. Al die data komt in het item in `albums.json` in de property
'discogs' te staan.

De Discogs data wordt gezocht op basis van artiest en album naam. Vaak gaat dat
fout omdat de verkeerde versie van het album gevonden wordt. De LP in plaats van
de CD bijvoorbeeld. In dat geval moet met de hand een aanpassing in het JSON
bestand gedaan worden: zoek op discogs.com de juiste uitgave op. Haal de
'discogs' property uit de JSON weg en voeg de property 'discogsId' toe met als
waarde het ID van de uitgave. Start de app opnieuw. DiscogsID wordt nu gebruikt
om het album te zoeken in plaats van artiest en album naam, en de juiste uitgave
zal in het JSON bestand gezet worden.

# Discogs

If you just want to see some results right now, issue this curl command:

```bash
curl https://api.discogs.com/releases/15424883 --user-agent "FooBarApp/3.0"
```

## Documentation and tutorials

* Electron quick start tutorial
	* https://www.electronjs.org/docs/latest/tutorial/quick-start
* TypeScript and Electron The Right Way
	* https://davembush.medium.com/typescript-and-electron-the-right-way-141c2e15e4e1
* Electron - Quick start with Typescript
	* https://dev.to/batajus/electron-quick-start-with-typescript-48h8
* Get all directories within directory nodejs
	* https://stackoverflow.com/questions/18112204/get-all-directories-within-directory-nodejs
* Disconnect
	* https://github.com/bartve/disconnect
* Custom App Icons
	* https://www.electronforge.io/guides/create-and-add-icons
* How to display a JPG image from a Node.js buffer (UInt8Array)
	* https://stackoverflow.com/questions/38503181/how-to-display-a-jpg-image-from-a-node-js-buffer-uint8array?rq=4
* Select and display an image from the filesystem with electron
	* https://stackoverflow.com/questions/50781741/select-and-display-an-image-from-the-filesystem-with-electron
* Adding TypeSafety to Electron IPC with TypeScript
	* https://kishannirghin.medium.com/adding-typesafety-to-electron-ipc-with-typescript-d12ba589ea6a

